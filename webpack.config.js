const path = require('path');

module.exports = {
    entry: {
        'accordion': './src/js/accordion.js',
        'tabs': './src/js/tabs.js',
        'blocks/accordion': './src/js/blocks/accordion/block.js',
        'blocks/accordion-item': './src/js/blocks/accordion-item/block.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist/js'),
    },
    optimization: {
        minimize: false
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
        ]
    }
};
