<?php
/**
 * Plugin Name: Ten-321 Accordion Block
 * Description: A WordPress block that implements an accordion-style component with infinite child accordion items
 * Author: cgrymala
 * Author URI: https://ten-321.com/
 * Version: 2020.11.1
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package Ten321-Accordion-Block
 */

namespace {
	// Exit if accessed directly.
	if ( ! defined( 'ABSPATH' ) ) {
		exit;
	}

	spl_autoload_register( function ( $class_name ) {
		if ( ! stristr( $class_name, 'Ten321\Accordion_Block\\' ) && ! stristr( $class_name, 'Ten321\Common\\' ) ) {
			return;
		}

		$class_name = str_replace( 'Ten321\Accordion_Block\\', 'Ten321\Accordion_Block\/classes/', $class_name );

		$filename = trailingslashit( plugin_dir_path( __FILE__ ) ) . 'lib/' . strtolower( str_replace( array(
				'\\',
				'_'
			), array( '/', '-' ), $class_name ) ) . '.php';

		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			error_log( 'Attempting to find class definition at: ' . $filename );
		}

		if ( ! file_exists( $filename ) ) {
			return;
		}

		include $filename;
	} );
}

namespace Ten321\Accordion_Block {
	Plugin::instance();
}
