=== Ten-321 Accordion Block ===
Contributors: cgrymala
Tags: accordion, block, collapsible
Requires at least: 5.5
Tested up to: 5.6
Stable tag: 2020.11.1
Requires PHP: 7.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin implements an accessible accordion block with unlimited accordion items inside.

== Description ==

This plugin implements an accessible accordion block, based on the example found on the [Carnegie Museums accessibility guidelines site](http://web-accessibility.carnegiemuseums.org/code/accordions/).

To use the Accordion block, simply insert a new Accordion block in the editor, then begin inserting Accordion Item blocks. Within each Accordion Item, you will need to specify a title (which is used as the heading/handle of the accordion), and you can specify whether or not the accordion item should be open or closed by default. Beyond that, you can insert any block you choose into the content area of the Accordion Item.

== Frequently Asked Questions ==

= Is this accordion accessible? =

This accordion, itself, should be accessible to everyone. However, any content placed inside of the accordion items will still need to be reviewed separately.

= What types of content can I use inside of an accordion item? =

The plugin itself does not limit which blocks can be inserted into an accordion item. However, you can use the `ten321/accordion-block/accordion-item/allowed-blocks` filter to list allowed blocks if you would like to limit what is allowed inside of accordion items.

= Can I use more than one Accordion Block on a page? =

Yes, you can insert as many separate Accordion Blocks as you want within any page.

== Screenshots ==

1. What the Accordion Item looks like while you are editing the title.
2. What the Accordion Item looks like while you are editing blocks inside of it.

== Changelog ==

= 2020.10.1 =
* Initial release

== Upgrade Notice ==

= 2020.10.1 =
This is the first version of this plugin
