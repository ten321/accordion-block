<?php

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace Ten321\Accordion_Block {

	if ( ! class_exists( 'Plugin' ) ) {
		class Plugin {
			/**
			 * @var Plugin $instance holds the single instance of this class
			 * @access private
			 */
			private static $instance;
			/**
			 * @var string $version holds the version number for the plugin
			 * @access public
			 */
			public static $version = '2020.11.1.06';
			/**
			 * @var array $tab_handles holds the list of tab indices and titles
			 * @access private
			 */
			private $tab_handles = array();

			/**
			 * Count the number of accordions in use
			 */
			private $accordion_count = 1;

			/**
			 * Count the number of accordion items in use
			 */
			private $accordion_item_count = 0;

			/**
			 * Keep track of all unique accordions and their items
			 */
			private $accordion_ids = array();

			/**
			 * Creates the Plugin object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				add_action( 'init', array( $this, 'block_assets' ) );
				add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  Plugin
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Custom logging function that can be short-circuited
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public static function log( $message ) {
				if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
					return;
				}

				error_log( '[Accordion Block Debug]: ' . $message );
			}

			/**
			 * Retrieve a URL relative to the root of this plugin
			 *
			 * @param string $path the path to append to the root plugin path
			 *
			 * @access public
			 * @return string the full URL to the provided path
			 * @since  0.1
			 */
			public static function plugins_url( $path ) {
				return plugins_url( $path, dirname( __FILE__, 4 ) );
			}

			/**
			 * Retrieve and return the root path of this plugin
			 *
			 * @access public
			 * @return string the absolute path to the root of this plugin
			 * @since  0.1
			 */
			public static function plugin_dir_path() {
				return plugin_dir_path( dirname( __FILE__, 4 ) );
			}

			/**
			 * Retrieve and return the root URL of this plugin
			 *
			 * @access public
			 * @return string the absolute URL
			 * @since  0.1
			 */
			public static function plugin_dir_url() {
				return plugin_dir_url( dirname( __FILE__, 4 ) );
			}

			/**
			 * Set up the block assets
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public function block_assets() {
				// Register block styles for both frontend + backend.
				wp_register_style(
					'ten321-accordion-block-style-css', // Handle.
					self::plugins_url( 'dist/css/accordion-block.min.css' ), // Block style CSS.
					is_admin() ? array( 'wp-editor', 'dashicons' ) : null, // Dependency to include the CSS after it.
					self::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
					'all'
				);

				// Register block editor script for backend.
				wp_register_script(
					'ten321-accordion-block-js', // Handle.
					self::plugins_url( '/dist/js/blocks/accordion.min.js' ), // Block.build.js: We register the block here. Built with Webpack.
					array(
						'wp-blocks',
						'wp-block-editor',
						'wp-i18n',
						'wp-element',
						'wp-editor',
						'wp-components',
						'wp-compose',
					), // Dependencies, defined above.
					self::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
					true // Enqueue the script in the footer.
				);

				// Register block editor script for backend.
				wp_register_script(
					'ten321-accordion-item-block-js', // Handle.
					self::plugins_url( '/dist/js/blocks/accordion-item.min.js' ), // Block.build.js: We register the block here. Built with Webpack.
					array(
						'wp-blocks',
						'wp-block-editor',
						'wp-i18n',
						'wp-element',
						'wp-editor',
						'wp-components',
						'wp-compose',
					), // Dependencies, defined above.
					self::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: filemtime — Gets file modification time.
					true // Enqueue the script in the footer.
				);

				wp_register_script(
					'ten321-accordion-block-frontend',
					self::plugins_url( '/dist/js/accordion.js' ),
					array(),
					self::$version,
					true
				);

				wp_register_script(
					'ten321-tab-block-frontend',
					self::plugins_url( '/dist/js/tabs.js' ),
					array(),
					self::$version,
					true
				);

				// Register block editor styles for backend.
				wp_register_style(
					'ten321-accordion-block-editor-css', // Handle.
					self::plugins_url( 'dist/css/accordion-block-editor.min.css' ), // Block editor CSS.
					array( 'wp-edit-blocks' ), // Dependency to include the CSS after it.
					self::$version, // filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
					'all'
				);

				// WP Localized globals. Use dynamic PHP stuff in JavaScript via `cgbGlobal` object.
				wp_localize_script(
					'ten321-accordion-block-js',
					'ten321_accordion_block_global', // Array containing dynamic data for a JS Global.
					[
						'pluginDirPath'      => self::plugin_dir_path(),
						'pluginDirUrl'       => self::plugin_dir_url(),
						'restURL'            => get_rest_url( $GLOBALS['blog_id'], '/ten321/v1/accordion-block/' ),
						'accordionItemCount' => 0,
						'allowedInnerBlocks' => apply_filters( 'ten321/accordion-block/accordion-item/allowed-blocks', array() ),
						// Add more data here that you want to access from `cgbGlobal` object.
					]
				);

				/**
				 * Register Gutenberg block on server-side.
				 *
				 * Register the block on server-side to ensure that the block
				 * scripts and styles for both frontend and backend are
				 * enqueued when the editor loads.
				 *
				 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
				 * @since 1.16.0
				 */
				register_block_type(
					'ten321/accordion-block', array(
						// Enqueue blocks.style.build.css on both frontend & backend.
						'style'           => 'ten321-accordion-block-style-css',
						// Enqueue blocks.build.js in the editor only.
						'editor_script'   => 'ten321-accordion-block-js',
						// Enqueue blocks.editor.build.css in the editor only.
						'editor_style'    => 'ten321-accordion-block-editor-css',
						'render_callback' => array( $this, 'render_accordion' ),
					)
				);

				/**
				 * Register Gutenberg block on server-side.
				 *
				 * Register the block on server-side to ensure that the block
				 * scripts and styles for both frontend and backend are
				 * enqueued when the editor loads.
				 *
				 * @link https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type#enqueuing-block-scripts
				 * @since 1.16.0
				 */
				register_block_type(
					'ten321/accordion-item', array(
						// Enqueue blocks.build.js in the editor only.
						'editor_script'   => 'ten321-accordion-item-block-js',
						'attributes'      => array(
							'title'      => array(
								'type' => 'string',
							),
							'open'       => array(
								'type' => 'boolean',
							),
							'introBlock' => array(
								'type' => 'boolean',
							)
						),
						'parent'          => array( 'ten321/accordion-block' ),
						'render_callback' => array( $this, 'render_accordion_item' ),
						'icon'            => 'align-wide',
					)
				);
			}

			public function enqueue_scripts() {
				return;
			}

			/**
			 * Render the Accordion block
			 *
			 * @param array $attributes the list of attributes associated with the block
			 * @param string $content the content of the block
			 * @param \WP_Block $block the original block object
			 *
			 * @access public
			 * @return string the rendered block content
			 * @since  0.1
			 */
			public function render_accordion( array $attributes, string $content, \WP_Block $block ) {
				$this->accordion_count ++;

				if ( stristr( $attributes['className'], 'is-style-tabs' ) ) {
					return $this->render_tabs( $attributes, $content, $block );
				}

				wp_enqueue_script( 'ten321-accordion-block-frontend' );

				$template_file = locate_template( 'block-templates/ten321/accordion-block/accordion.php' );
				if ( empty( $template_file ) ) {
					$template_file = self::plugin_dir_path() . '/lib/ten321/accordion-block/templates/block-accordion.php';
				}

				$template = file_get_contents( $template_file );

				$attributes = shortcode_atts( array(
					'align'   => 'left',
					'heading' => null,
					'accordionId' => '',
				), $attributes );

				if ( empty( $attributes['heading'] ) ) {
					$heading = '';
				} else {
					$heading = sprintf( '<h2>%s</h2>', $attributes['heading'] );
				}

				$replacements = apply_filters( 'ten321/accordion-block/accordion/template-replacements', array(
					'heading' => $heading,
					'content' => $content,
					'align'   => 'align-' . $attributes['align'],
				) );

				return vsprintf( $template, $replacements );
			}

			/**
			 * Render the Accordion Item block
			 *
			 * @param array $attributes the list of attributes associated with the block
			 * @param string $content the content of the block
			 * @param \WP_Block $block the original block object
			 *
			 * @access public
			 * @return string the rendered block content
			 * @since  0.1
			 */
			public function render_accordion_item( array $attributes, string $content, \WP_Block $block ) {
				$this->accordion_item_count ++;

				if ( stristr( $attributes['accordionStyle'], 'is-style-tabs' ) ) {
					return $this->render_tab_item( $attributes, $content, $block );
				}

				$template_file = locate_template( 'block-templates/ten321/accordion-block/accordion-item.php' );
				if ( empty( $template_file ) ) {
					$template_file = self::plugin_dir_path() . '/lib/ten321/accordion-block/templates/block-accordion-item.php';
				}

				$template = file_get_contents( $template_file );

				$attributes = shortcode_atts( array(
					'title'      => null,
					'open'       => false,
					'introBlock' => false,
					'index'      => $this->accordion_item_count(),
					'accordionId' => '',
				), $attributes );

				$firstblock = '';
				if ( $attributes['introBlock'] ) {
					self::log( print_r( $block->parsed_block['innerBlocks'], true ) );

					$firstblock = array_shift( $block->parsed_block['innerBlocks'] );

					self::log( 'First block looks like: ' . print_r( $firstblock, true ) );

					$firstblock = '<div class="accordion-item-intro">' . $firstblock['innerHTML'] . '</div>';

					$content = '';
					foreach ( $block->parsed_block['innerBlocks'] as $b ) {
						$content .= $b['innerHTML'];
					}
				}

				$index = 'accordion-item-' . $attributes['index'];
				if ( ! empty( $attributes['title'] ) ) {
					$index = sanitize_title( $attributes['title'] );
				}

				$replacements = apply_filters( 'ten321/accordion-block/accordion-item/template-replacements', array(
					'index'      => $index,
					'expanded'   => $attributes['open'] ? 'true' : 'false',
					'control'    => 'accordion-control-' . $attributes['index'],
					'hidden'     => $attributes['open'] ? 'false' : 'true',
					'content'    => $content,
					'title'      => $attributes['title'],
					'firstblock' => $firstblock,
				) );

				return vsprintf(
					$template,
					$replacements
				);
			}

			/**
			 * Render the Tab block
			 *
			 * @param array $attributes the list of attributes associated with the block
			 * @param string $content the content of the block
			 * @param \WP_Block $block the original block object
			 *
			 * @access public
			 * @return string the rendered block content
			 * @since  0.1
			 */
			public function render_tabs( array $attributes, string $content, \WP_Block $block ) {
				wp_enqueue_script( 'ten321-tab-block-frontend' );

				$template_file = locate_template( 'block-templates/ten321/accordion-block/tabs.php' );
				if ( empty( $template_file ) ) {
					$template_file = self::plugin_dir_path() . '/lib/ten321/accordion-block/templates/block-tabs.php';
				}

				$template = file_get_contents( $template_file );

				$attributes = shortcode_atts( array(
					'align'     => 'left',
					'heading'   => null,
					'className' => '',
					'accordionId' => '',
				), $attributes );

				if ( empty( $attributes['heading'] ) ) {
					$heading = '';
				} else {
					$heading = sprintf( '<h2>%s</h2>', $attributes['heading'] );
				}

				$tabsList = array();

				$index = 0;
				foreach ( $block->inner_blocks as $tab ) {
					$tabsList[] = $this->render_tab_handle( $attributes, $tab, $index );
					$index++;
				}

				$replacements = apply_filters( 'ten321/accordion-block/tabs/template-replacements', array(
					'heading'     => $heading,
					'tabsList'    => implode( '', $tabsList ),
					'tabsContent' => $content,
					'className'   => 'align-' . $attributes['align'] . ' ' . $attributes['className'],
				) );

				return vsprintf( $template, $replacements );
			}

			/**
			 * Render the Tab Item block
			 *
			 * @param array $attributes the list of attributes associated with the block
			 * @param string $content the content of the block
			 * @param \WP_Block $block the original block object
			 *
			 * @access public
			 * @return string the rendered block content
			 * @since  0.1
			 */
			public function render_tab_item( array $attributes, string $content, \WP_Block $block ) {
				$template_file = locate_template( 'block-templates/ten321/accordion-block/tab-panel.php' );
				if ( empty( $template_file ) ) {
					$template_file = self::plugin_dir_path() . '/lib/ten321/accordion-block/templates/block-tab-panel.php';
				}

				$template = file_get_contents( $template_file );

				$attributes = shortcode_atts( array(
					'title'          => null,
					'open'           => false,
					'accordionStyle' => 'tabs',
					'className'      => '',
					'accordionId' => '',
				), $attributes );

				$title = $attributes['title'];
				$slug  = sanitize_title( $title );

				if ( in_array( $slug, $this->tab_handles ) ) {
					$slug .= '-' . $this->accordion_item_count;
				}

				$this->accordion_ids[$attributes['accordionId']][] = array(
					'slug'      => $slug,
					'title'     => $title,
					'accordion' => $this->accordion_count,
				);

				$this->tab_handles[] = $slug;

				$replacements = apply_filters( 'ten321/accordion-block/tab-panel/template-replacements', array(
					'index'   => $slug,
					'current' => $attributes['open'] ? 'current' : 'hidden',
					'title'   => $title,
					'content' => $content,
				) );

				return vsprintf(
					$template,
					$replacements
				);
			}

			/**
			 * Render the Tab Handle
			 *
			 * @param array $attributes the block attributes
			 * @param \WP_Block $block the block object
			 * @param int $index the index of the current tab block
			 *
			 * @access public
			 * @return string the rendered tab handle
			 * @since  0.1
			 */
			public function render_tab_handle( array $attributes, \WP_Block $block, $index=0 ) {
				$handle = $this->accordion_ids[$attributes['accordionId']][$index];

				$slug = $handle['slug'];
				$title = $handle['title'];

				$open = $block->parsed_block['attrs']['open'] || false;

				$template_file = locate_template( 'block-templates/ten321/accordion-block/tab-handle.php' );
				if ( empty( $template_file ) ) {
					$template_file = self::plugin_dir_path() . '/lib/ten321/accordion-block/templates/block-tab-handle.php';
				}
				$template = file_get_contents( $template_file );

				$replacements = apply_filters( 'ten321/accordion-block/tab-handle/template-replacements', array(
					'index'    => $slug,
					'selected' => (string) $open,
					'title'    => $title,
					'containerClass' => 'tab-handle' . ( $open ? ' current' : '' ),
					'className' => 'tab' . ( $open ? ' selected' : '' ),
					'tabIndex' => $open ? '0' : '-1',
				) );

				return vsprintf( $template, $replacements );
			}

			/**
			 * Generate a unique index for each accordion item
			 *
			 * @access private
			 * @return int the unique index for the accordion item
			 * @since  0.1
			 */
			private function accordion_item_count() {
				return sprintf( '%d-%d', $this->accordion_count, $this->accordion_item_count );
			}
		}
	}
}
