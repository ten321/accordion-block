<li class="accordion-item">
    <button class="accordion-control-button" aria-controls="%1$s" aria-expanded="%2$s" id="%3$s">%6$s</button>
    <div class="accordion-item-content-wrapper">
    %7$s
    <div aria-hidden="%4$s" id="%1$s" class="accordion-item-content">
        %5$s
    </div>
</li>
