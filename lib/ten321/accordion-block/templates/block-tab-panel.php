<div aria-labelledby="tab-%1$s" class="tab-panel %2$s" id="%1$s" role="tabpanel">
    <h2>%3$s</h2>
    <div class="tab-panel-content">
        %4$s
    </div>
</div>
