/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ 7:
/***/ (function(module, exports) {

var ten321Tabs = {
  index: 0,
  tabs: [],
  tabGroups: [],
  init: function init() {
    this.tabGroups = document.querySelectorAll('.tabbed-content');
    this.tabs = document.querySelectorAll('a.tab');
    this.tabGroups.forEach(function (group) {
      if (group.querySelectorAll('a.tab.selected').length <= 0) {
        var first = group.querySelector('.tab-handle:first-child a.tab');
        ten321Tabs.setFocus(group, first);
      }

      if (group.querySelectorAll('a.tab.selected').length > 1) {
        var _first = group.querySelectorAll('.tab-handle.current a.tab')[0];
        ten321Tabs.setFocus(group, _first);
      }

      var tabs = group.querySelectorAll('a.tab');
      group.setAttribute('data-max-height', 0);
      tabs.forEach(function (tab) {
        var tabContent = document.querySelector(tab.getAttribute('href'));
        var isHidden = tabContent.classList.contains('hidden');
        tabContent.classList.remove('hidden');
        tabContent.classList.add('current');
        tabContent.style.maxHeight = 'initial';
        var compStyle = getComputedStyle(tabContent);

        if (compStyle.getPropertyValue('height') > tab.closest('.tabbed-content').getAttribute('data-max-height')) {
          tab.closest('.tabbed-content').setAttribute('data-max-height', compStyle.getPropertyValue('height'));
        }

        tabContent.style.maxHeight = '';

        if (isHidden) {
          tabContent.classList.add('hidden');
          tabContent.classList.remove('current');
        }

        tab.addEventListener('keydown', ten321Tabs.keyDown);
        tab.addEventListener('click', ten321Tabs.click);
      });
      group.querySelectorAll('.tab-panel').forEach(function (panel) {
        var tabGroup = group.closest('.tabbed-content');
        panel.style.height = tabGroup.getAttribute('data-max-height');
      });
    });
  },
  keyDown: function keyDown(ev) {
    var selected = ev.target;
    var LEFT_ARROW = 37;
    var UP_ARROW = 38;
    var RIGHT_ARROW = 39;
    var DOWN_ARROW = 40;
    var k = ev.which || ev.keyCode;
    var tabGroup = selected.closest('.tabbed-content');
    var current = tabGroup.querySelector('a.tab[aria-selected="true"]');
    var tabs = tabGroup.querySelectorAll('a.tab');
    var newTab = null; // if the key pressed was an arrow key

    if (k >= LEFT_ARROW && k <= DOWN_ARROW) {
      // move left one tab for left and up arrows
      if (k === LEFT_ARROW || k === UP_ARROW) {
        if (tabs.item(0) === current) {
          newTab = tabGroup.querySelector('.tab-handle:last-child a.tab');
        } else {
          var tmp = current.parentNode.previousElementSibling;
          newTab = tmp.querySelector('a.tab');
        }
      } // move right one tab for right and down arrows
      else if (k === RIGHT_ARROW || k === DOWN_ARROW) {
          if (tabGroup.querySelector('.tab-handle:last-child a.tab') === current) {
            newTab = tabGroup.querySelector('.tab-handle:first-child a.tab');
          } else {
            var _tmp = current.parentNode.nextElementSibling;
            newTab = _tmp.querySelector('a.tab');
          }
        } // trigger a click event on the tab to move to


      ten321Tabs.setFocus(tabGroup, newTab);
      ev.preventDefault();
    }
  },
  click: function click(ev) {
    ten321Tabs.setFocus(ev.target.closest('.tabbed-content'), ev.target);
    ev.preventDefault();
    return false;
  },
  setFocus: function setFocus(tabGroup, selected) {
    var tabs = tabGroup.querySelectorAll('a.tab');
    var panels = tabGroup.querySelectorAll('.tab-panel'); // undo tab control selected state,
    // and make them not selectable with the tab key
    // (all tabs)

    tabs.forEach(function (tab) {
      tab.setAttribute('tabindex', '-1');
      tab.setAttribute('aria-selected', 'false');
      tab.classList.remove('selected');
      tab.closest('li').classList.remove('current');
    });
    panels.forEach(function (panel) {
      panel.classList.remove('current');
      panel.classList.add('hidden');
    }); // make the selected tab the selected one, shift focus to it

    selected.setAttribute('tabindex', '0');
    selected.setAttribute('aria-selected', 'true');
    selected.classList.add('selected');
    selected.focus();
    selected.closest('li').classList.add('current');
    document.querySelector(selected.getAttribute('href')).classList.add('current');
    document.querySelector(selected.getAttribute('href')).classList.remove('hidden');
    return false;
  }
};
document.addEventListener('DOMContentLoaded', function () {
  ten321Tabs.init();
});

/***/ })

/******/ });