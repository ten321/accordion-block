/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ 6:
/***/ (function(module, exports) {

var ten321Accordion = {
  accordions: null,
  init: function init() {
    var openItem = 0;

    if (window.location.hash) {
      openItem = window.location.hash.replace('#', ''); // Check to make sure the hash refers to an accordion item

      if (!document.getElementById(openItem).classList.contains('accordion-item-content')) {
        openItem = 0;
      }
    }

    this.accordions = document.querySelectorAll('.ten321-accordion');
    this.accordions.forEach(function (accordion) {
      if (accordion.classList.contains('tabbed-content')) {
        return false;
      }

      var items = accordion.querySelectorAll('.accordion-item');
      var openFirstItem = true;

      if (openItem !== 0 && accordion.querySelectorAll('#' + openItem).length > 0) {
        openFirstItem = false;
      } else if (accordion.querySelectorAll('[aria-expanded="true"]').length > 0) {
        openItem = accordion.querySelectorAll('[aria-expanded="true"]')[0].id;
        openFirstItem = false;
      }

      items.forEach(function (item) {
        ten321Accordion.setUpAccordionItem(item, {
          openItem: openItem,
          openFirstItem: openFirstItem
        });
        var handles = accordion.querySelectorAll('.accordion-control-button');
        handles.forEach(function (handle) {
          ten321Accordion.setClickEvents(handle);
        });
      });

      if (openFirstItem) {
        ten321Accordion.openItem(accordion.querySelectorAll('.accordion-item')[0]);
      } else if (openItem !== 0) {
        ten321Accordion.openItem(accordion.querySelector('#' + openItem).closest('.accordion-item'));
        ten321Accordion.scrollTo();
      }
    });
    return false;
  },
  hashChanged: function hashChanged() {
    if (!window.location.hash) {
      return false;
    }

    var openItem = window.location.hash.replace('#', '');
    var openItemContent = document.getElementById(openItem);
    var accordion = openItemContent.closest('.ten321-accordion');
    var items = accordion.querySelectorAll('.accordion-item');
    var openFirstItem = true;

    if (openItem !== 0 && accordion.querySelectorAll('#' + openItem).length > 0) {
      openFirstItem = false;
    } else if (accordion.querySelectorAll('[aria-expanded="true"]').length > 0) {
      openFirstItem = false;
    }

    items.forEach(function (item) {
      ten321Accordion.closeItem(item);
    });
    ten321Accordion.openItem(openItemContent.closest('.accordion-item'));
  },
  setUpAccordionItem: function setUpAccordionItem(item, atts) {
    var openItem = 0;
    var openFirstItem = true;

    if ('openItem' in atts) {
      openItem = atts.openItem;
    }

    if ('openFirstItem' in atts) {
      openFirstItem = atts.openFirstItem;
    }

    var accordion = item.closest('.ten321-accordion');
    var highestHeight = accordion.getAttribute('data-max-height');
    var currentContent = item.querySelector('.accordion-item-content');
    var isExpanded = item.querySelector('.accordion-control-button').getAttribute('aria-expanded') === 'true';

    if (0 !== openItem) {
      isExpanded = openItem === currentContent.id;
    } else if (openFirstItem && accordion.querySelectorAll('.accordion-item:first-child') === item) {
      isExpanded = true;
    } // Start with all items expanded, so we can measure their height; if any need to be closed, we'll do that later


    item.querySelector('.accordion-control-button').setAttribute('aria-expanded', 'true');
    item.querySelector('.accordion-item-content').setAttribute('aria-hidden', 'false');
    currentContent.setAttribute('data-max-height', currentContent.offsetHeight);

    if (currentContent.offsetHeight > highestHeight) {
      highestHeight = currentContent.offsetHeight;
      accordion.setAttribute('data-max-height', highestHeight);
    }

    ten321Accordion.closeItem(item);
  },
  closeItem: function closeItem(item) {
    item.querySelector('.accordion-item-content').style.maxHeight = 0;
    item.querySelector('.accordion-control-button').setAttribute('aria-expanded', 'false');
    item.querySelector('.accordion-item-content').setAttribute('aria-hidden', 'true');
    item.querySelector('.accordion-item-content').removeEventListener('transitionend', ten321Accordion.transitionFinished);
  },
  openItem: function openItem(item) {
    if (typeof item === 'undefined') {
      return;
    }

    var itemContentElement = item.querySelector('.accordion-item-content');
    itemContentElement.addEventListener('transitionend', ten321Accordion.transitionFinished);
    item.querySelector('.accordion-control-button').setAttribute('aria-expanded', 'true');
    itemContentElement.setAttribute('aria-hidden', 'false');

    if (itemContentElement.getAttribute('data-max-height') > 0) {
      itemContentElement.style.maxHeight = itemContentElement.getAttribute('data-max-height') + 'px';
    } else {
      itemContentElement.style.maxHeight = 'initial';
    }
  },
  setClickEvents: function setClickEvents(handle) {
    handle.removeEventListener('click', ten321Accordion.doClick);
    handle.addEventListener('click', ten321Accordion.doClick);
  },
  doClick: function doClick(e) {
    var control = e.target;
    var accordion = control.closest('.ten321-accordion');
    var items = accordion.querySelectorAll('.accordion-item');
    var currentItem = control.closest('.accordion-item');
    var currentContent = currentItem.querySelector('.accordion-item-content');
    var highestHeight = accordion.getAttribute('data-max-height');
    var isClosed = currentContent.getAttribute('aria-hidden') === 'true';
    var state = {
      pageId: window.location.href.split("#")[0],
      accordionId: Array.prototype.indexOf.call(document.querySelectorAll('.ten321-accordion'), accordion),
      itemId: Array.prototype.indexOf.call(items, currentItem),
      htmlId: currentContent.id
    };
    items.forEach(function (item) {
      ten321Accordion.closeItem(item);
    });

    if (isClosed) {
      currentContent.addEventListener('transitionend', ten321Accordion.transitionFinished);
      ten321Accordion.openItem(currentItem);
      history.pushState(state, control.innerText, '#' + state.htmlId);
    }
  },
  getScrollTop: function getScrollTop(item) {
    if (typeof item === 'undefined') {
      return false;
    }

    var headers = document.querySelectorAll('.site-header');
    var stickyHeader = false;
    headers.forEach(function (header) {
      var compStyle = getComputedStyle(header);
      var stylePosition = compStyle.getPropertyValue('position');

      if (stylePosition === 'fixed') {
        stickyHeader = header;
      }
    });
    var abOffset = 0;
    var adminBar = document.getElementById('wpadminbar');

    if (null !== adminBar) {
      var abStyle = getComputedStyle(adminBar);
      var abPosition = abStyle.getPropertyValue('position');

      if (abPosition === 'fixed') {
        abOffset = adminBar.offsetHeight;
      }
    }

    var pos = window.pageYOffset + item.getBoundingClientRect().top;

    if (stickyHeader !== false) {
      pos -= stickyHeader.offsetHeight;
    }

    pos -= abOffset;
    return pos;
  },
  scrollTo: function scrollTo() {
    if (!window.location.hash) {
      return;
    }

    var item = document.getElementById(window.location.hash.replace('#', '')).closest('.accordion-item');

    if (typeof item === 'undefined') {
      return;
    }

    var scrollTop = ten321Accordion.getScrollTop(item);

    if (false === scrollTop || isNaN(scrollTop)) {
      return;
    }

    window.scrollTo({
      top: scrollTop,
      behavior: 'smooth'
    });
    item.querySelector('.accordion-control-button').focus();
  },
  transitionFinished: function transitionFinished(e) {
    var openItem = e.target.closest('.accordion-item');
    e.target.removeEventListener('transitionend', ten321Accordion.transitionFinished);
    ten321Accordion.scrollTo();
  },
  popStateChanged: function popStateChanged(e) {
    if (typeof e === 'undefined') {
      ten321Accordion.hashChanged();
      return false;
    }

    if ('htmlId' in e.state) {
      var accordion = document.getElementById(e.state.htmlId).closest('.ten321-accordion');
      var currentItem = document.getElementById(e.state.htmlId).closest('.accordion-item');
      accordion.querySelectorAll('.accordion-item').forEach(function (item) {
        ten321Accordion.closeItem(item);
      });
      ten321Accordion.openItem(currentItem);
      ten321Accordion.scrollTo();
    }
  }
};
document.addEventListener('DOMContentLoaded', function () {
  ten321Accordion.init();
});
window.onhashchange = ten321Accordion.hashChanged();
window.addEventListener('popstate', function () {
  ten321Accordion.popStateChanged();
});

/***/ })

/******/ });