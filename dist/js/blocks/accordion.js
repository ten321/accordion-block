/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && typeof btoa !== 'undefined') {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(0);
            var content = __webpack_require__(3);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(1);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".item-closed,.wp-block-ten321-accordion-block.is-style-tabs .accordion-item-title,[data-type=\"ten321/accordion-block\"]:not(.has-child-selected) .is-style-tabs [data-type=\"ten321/accordion-item\"],[data-type=\"ten321/accordion-block\"].has-child-selected .is-style-tabs [data-type=\"ten321/accordion-item\"]:not(.is-selected):not(.has-child-selected){width:0;height:0;margin:0;padding:0;overflow:hidden;display:block}.item-open,[data-type=\"ten321/accordion-block\"]:not(.has-child-selected) .is-style-tabs [data-type=\"ten321/accordion-item\"]:first-child,[data-type=\"ten321/accordion-block\"].has-child-selected .is-style-tabs .is-selected[data-type=\"ten321/accordion-item\"],[data-type=\"ten321/accordion-block\"].has-child-selected .is-style-tabs .has-child-selected[data-type=\"ten321/accordion-item\"],[data-type=\"ten321/accordion-block\"].has-child-selected .is-style-tabs [data-type=\"ten321/accordion-item\"].is-selected .accordion-item-title,[data-type=\"ten321/accordion-block\"].has-child-selected .is-style-tabs [data-type=\"ten321/accordion-item\"].has-child-selected .accordion-item-title{width:100%;height:auto;margin:0 auto;padding:1rem;overflow:visible;display:block}[data-type=\"ten321/accordion-block\"]{width:100%;max-width:100%}[data-type=\"ten321/accordion-block\"] .is-style-accordion .tab-handles{display:none}.wp-block-ten321-accordion-block ul.accordion-controls{list-style:none;margin:0 0 1rem;padding:.5rem;border:1px solid #e2e2e2}.wp-block-ten321-accordion-block ul.accordion-controls .accordion-item-title h4,.wp-block-ten321-accordion-block ul.accordion-controls .components-panel>.components-panel__row{border:1px solid #333;display:block;width:100%;max-width:100%;padding:.5rem 1rem;text-align:left;font-size:1rem;font-weight:bold}.wp-block-ten321-accordion-block ul.accordion-controls .accordion-item-title h4:after,.wp-block-ten321-accordion-block ul.accordion-controls .components-panel>.components-panel__row:after{display:inline-block;float:right;content:\"\\f347\";font-family:'dashicons'}.wp-block-ten321-accordion-block ul.accordion-controls .components-panel>.components-panel__row:after{content:none}.wp-block-ten321-accordion-block ul.accordion-controls li.wp-block-ten321-accordion-item{list-style:none;margin:1rem 0;padding:0 0 1rem;border-bottom:1px solid #000}.wp-block-ten321-accordion-block ul.accordion-controls [data-type=\"ten321/accordion-item\"] .block-editor-inner-blocks{display:none}.wp-block-ten321-accordion-block ul.accordion-controls [data-type=\"ten321/accordion-item\"].has-child-selected .block-editor-inner-blocks,.wp-block-ten321-accordion-block ul.accordion-controls [data-type=\"ten321/accordion-item\"].is-selected .block-editor-inner-blocks{display:block}.wp-block-ten321-accordion-block ul.accordion-controls [data-type=\"ten321/accordion-item\"].has-child-selected .accordion-item-title h4:after,.wp-block-ten321-accordion-block ul.accordion-controls [data-type=\"ten321/accordion-item\"].is-selected .accordion-item-title h4:after{content:\"\\f343\"}.wp-block-ten321-accordion-block.is-style-tabs .tab-handles{display:block;padding:0;margin:0;background:#eee;border:1px solid #000;border-top-left-radius:10px;border-top-right-radius:10px}.wp-block-ten321-accordion-block.is-style-tabs .tab-handles .tab-handle{display:inline-block;padding:.5rem 1rem;background:transparent;border:1px solid #000;margin:-1px;border-top-left-radius:10px;border-top-right-radius:10px}.wp-block-ten321-accordion-block.is-style-tabs .accordion-item-title{margin:0 !important;padding:0 0 1rem 0 !important}.wp-block-ten321-accordion-block.is-style-tabs .accordion-item-title h4{border:none !important;margin:0 !important;padding:0 !important}.wp-block-ten321-accordion-block.is-style-tabs .accordion-item-title h4:after{content:none !important}.wp-block-ten321-accordion-block.is-style-tabs .accordion-item-title h4:before{content:\"Title: \";font-style:italic;color:#999;display:inline}.wp-block-ten321-accordion-block.is-style-tabs .block-editor-inner-blocks{display:block !important}.wp-block-ten321-accordion-block.is-style-tabs li.wp-block-ten321-accordion-item{border:none !important;margin:0;padding:0}\n", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(0);
            var content = __webpack_require__(5);

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(1);
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, ".ten321-accordion{display:block;list-style:none;margin:0;padding:0;border:none}.ten321-accordion li.accordion-item{list-style:none;margin:0 0 1rem;padding:0}.ten321-accordion button.accordion-control-button{border:1px solid #333;display:block;width:100%;max-width:100%;padding:.5rem 1rem;text-align:left;font-size:1rem;font-weight:bold}.ten321-accordion button.accordion-control-button:focus{outline:none}.ten321-accordion button.accordion-control-button:after{display:inline-block;float:right;content:\"\\f347\";font-family:'dashicons'}.ten321-accordion button.accordion-control-button[aria-expanded=\"true\"]:after{content:\"\\f343\"}.ten321-accordion .accordion-item-content-wrapper{display:block;margin:0;padding:.5rem 1rem;width:100%;max-width:100%;text-align:left;font-size:.875rem;font-weight:normal}.ten321-accordion .accordion-item-content-wrapper .accordion-item-content{height:auto;transition:max-height 0.3s linear}.ten321-accordion .accordion-item-content-wrapper .accordion-item-content:focus{outline:none}.ten321-accordion .accordion-item-content-wrapper .accordion-item-content[aria-hidden=\"true\"]{max-height:0;overflow:hidden}.ten321-accordion.tabbed-content{display:grid;grid-template-columns:1fr;grid-template-areas:\"handles\" \"tabs\";padding:0;margin:0 0 1rem;border:1px solid #000;border-top-left-radius:4px;border-top-right-radius:4px}.ten321-accordion.tabbed-content ul.tabs-list{list-style:none;display:flex;flex-direction:row;flex-wrap:wrap;align-content:flex-start;align-items:flex-start;justify-content:flex-start;background:#e2e2e2;border-top-left-radius:4px;border-top-right-radius:4px;padding:0;margin:0;grid-area:handles;border-bottom:1px solid #000}.ten321-accordion.tabbed-content ul.tabs-list li{list-style:none;margin:0;padding:0}.ten321-accordion.tabbed-content ul.tabs-list li:before{content:none}.ten321-accordion.tabbed-content ul.tabs-list li a.tab{display:inline-block;margin:-1px 0 -1px -1px;padding:.25rem .5rem;border:1px solid #000;border-top-left-radius:5px;border-top-right-radius:5px;background:#333;color:#fff}.ten321-accordion.tabbed-content ul.tabs-list li a.tab.selected{border-bottom:1px solid #e2e2e2}.ten321-accordion.tabbed-content ul.tabs-list li a.tab:focus{outline:none}.ten321-accordion.tabbed-content ul.tabs-list li a.tab:hover,.ten321-accordion.tabbed-content ul.tabs-list li a.tab:focus,.ten321-accordion.tabbed-content ul.tabs-list li a.tab.selected{background:transparent;color:#333}.ten321-accordion.tabbed-content .tab-panel{grid-area:tabs;margin:0;padding:1rem;opacity:0;max-height:0;overflow:hidden;transition:max-height 150ms ease-in-out, opacity 250ms ease-in-out}.ten321-accordion.tabbed-content .tab-panel.current{opacity:1;max-height:initial}\n", ""]);
// Exports
module.exports = exports;


/***/ }),
/* 6 */,
/* 7 */,
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scss_accordion_block_editor_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);
/* harmony import */ var _scss_accordion_block_editor_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_accordion_block_editor_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _scss_accordion_block_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4);
/* harmony import */ var _scss_accordion_block_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_scss_accordion_block_scss__WEBPACK_IMPORTED_MODULE_1__);


var __ = wp.i18n.__; // Import __() from wp.i18n

var registerBlockType = wp.blocks.registerBlockType; // Import registerBlockType() from wp.blocks

var InnerBlocks = wp.blockEditor.InnerBlocks;
var accordionItemCount = 0;
var BLOCKS_TEMPLATE = [['ten321/accordion-item', {}]];
registerBlockType('ten321/accordion-block', {
  // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
  title: __('Accordion Block', 'ten321/accordion-block'),
  // Block title.
  icon: 'menu',
  // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
  category: 'layout',
  // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
  keywords: [__('Accordion', 'ten321/accordion-block'), __('Collapse', 'ten321/accordion-block'), __('Tabs', 'ten321/accordion-block')],
  styles: [{
    name: 'accordion',
    label: __('Accordion', 'ten321/accordion-block'),
    isDefault: true
  }, {
    name: 'tabs',
    label: __('Tabs', 'ten321/accordion-block')
  }],
  providesContext: {
    'ten321/accordion-block/style': 'className',
    'ten321/accordion-block/accordion-id': 'accordionId',
    'ten321/accordion-block/block': undefined
  },
  attributes: {
    accordionId: {
      type: 'string'
    }
  },
  supports: {
    align: true
  },

  /**
   * The edit function describes the structure of your block in the context of the editor.
   * This represents what the editor will render when the block is used.
   *
   * The "edit" property must be a valid function.
   *
   * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
   *
   * @param {Object} props Props.
   * @returns {Mixed} JSX Component.
   */
  edit: function edit(_ref) {
    var className = _ref.className,
        isSelected = _ref.isSelected,
        attributes = _ref.attributes,
        setAttributes = _ref.setAttributes;
    var allowedBlocks = ['ten321/accordion-item'];
    var accordionId = attributes.accordionId;

    if (!accordionId) {
      setAttributes({
        accordionId: guid()
      });
    }

    className = className + ' ten321-accordion accordion-block-' + accordionId;

    function guid() {
      var s4 = function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
      }; //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'


      return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    return /*#__PURE__*/React.createElement("div", {
      className: className
    }, /*#__PURE__*/React.createElement("ul", {
      className: "accordion-controls"
    }, /*#__PURE__*/React.createElement(InnerBlocks, {
      allowedBlocks: allowedBlocks,
      template: BLOCKS_TEMPLATE
    })));
  },
  save: function save(_ref2) {
    var className = _ref2.className;
    return /*#__PURE__*/React.createElement(InnerBlocks.Content, null);
  }
});

/***/ })
/******/ ]);