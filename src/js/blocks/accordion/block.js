import '../../../scss/accordion-block-editor.scss';
import '../../../scss/accordion-block.scss';

const {__} = wp.i18n; // Import __() from wp.i18n
const {registerBlockType} = wp.blocks; // Import registerBlockType() from wp.blocks
const {InnerBlocks} = wp.blockEditor;
let accordionItemCount = 0;

const BLOCKS_TEMPLATE = [
    [ 'ten321/accordion-item', {} ]
];

registerBlockType('ten321/accordion-block', {
    // Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
    title: __('Accordion Block','ten321/accordion-block'), // Block title.
    icon: 'menu', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
    category: 'layout', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
    keywords: [
        __('Accordion','ten321/accordion-block'),
        __('Collapse','ten321/accordion-block'),
        __('Tabs','ten321/accordion-block'),
    ],
    styles: [
        {
            name: 'accordion',
            label: __( 'Accordion', 'ten321/accordion-block' ),
            isDefault: true,
        },
        {
            name: 'tabs',
            label: __( 'Tabs', 'ten321/accordion-block' ),
        },
    ],
    providesContext: {
        'ten321/accordion-block/style': 'className',
        'ten321/accordion-block/accordion-id': 'accordionId',
        'ten321/accordion-block/block': this,
    },
    attributes: {
        accordionId: {
            type: 'string',
        }
    },
    supports: {
        align: true,
    },
    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     *
     * @param {Object} props Props.
     * @returns {Mixed} JSX Component.
     */
    edit: ({className, isSelected, attributes, setAttributes}) => {
        const allowedBlocks = ['ten321/accordion-item'];
        const {accordionId} = attributes;
        if ( ! accordionId ) {
            setAttributes({accordionId: guid()});
        }
        className = className + ' ten321-accordion accordion-block-' + accordionId;

        function guid() {
            let s4 = () => {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }

        return (
            <div className={className}>
                <ul className="accordion-controls">
                    <InnerBlocks allowedBlocks={allowedBlocks} template={BLOCKS_TEMPLATE}/>
                </ul>
            </div>
        );
    },
    save: ({className}) => {
        return (
            <InnerBlocks.Content/>
        );
    }
})
