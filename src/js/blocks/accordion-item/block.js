import '../../../scss/accordion-block-editor.scss';
import '../../../scss/accordion-block.scss';

const {__} = wp.i18n; // Import __() from wp.i18n
const {registerBlockType} = wp.blocks; // Import registerBlockType() from wp.blocks
const {InnerBlocks, InspectorControls} = wp.blockEditor;
const {TextControl, ToggleControl, Panel, PanelBody, PanelRow} = wp.components;
const {withState} = wp.compose;

const allowedBlocks = ten321_accordion_block_global.allowedInnerBlocks;

const BLOCKS_TEMPLATE = [
    ['core/paragraph', {placeholder: __('Add content…', 'ten321/accordion-block')}],
];

registerBlockType('ten321/accordion-item', {
    title: __('Accordion Item', 'ten321/accordion-block'),
    icon: 'align-wide',
    category: 'layout',
    keywords: [
        __('Accordion', 'ten321/accordion-block'),
        __('Item', 'ten321/accordion-block'),
        __('Collapsible', 'ten321/accordion-block'),
    ],
    attributes: {
        title: {
            type: 'string',
        },
        open: {
            type: 'boolean',
        },
        introBlock: {
            type: 'boolean',
        },
        accordionStyle: {
            type: 'string',
        },
        accordionId: {
            type: 'string',
        },
        itemId: {
            type: 'string',
        },
        innerBlocks: [],
    },
    usesContext: ['ten321/accordion-block/style', 'ten321/accordion-block/accordion-id', 'ten321/accordion-block/block'],
    parent: ['ten321/accordion-block'],
    supports: {
        align: false,
    },
    /**
     * The edit function describes the structure of your block in the context of the editor.
     * This represents what the editor will render when the block is used.
     *
     * The "edit" property must be a valid function.
     *
     * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     *
     * @param {Object} props Props.
     * @returns {Mixed} JSX Component.
     */
    edit: ({attributes, setAttributes, className, isSelected, setState, context}) => {
        const {title, open, introBlock, accordionId, itemId, accordionStyle} = attributes;
        setAttributes({accordionId: context['ten321/accordion-block/accordion-id']});

        if ( ! itemId ) {
            setAttributes({itemId: guid()});
        }

        const style = context['ten321/accordion-block/style'];

        setAttributes({accordionStyle: style});

        const parentAccordion = document.querySelector('.accordion-block-' + accordionId);
        if ( parentAccordion && accordionStyle === 'is-style-tabs' ) {
            let handleContainer = parentAccordion.querySelector('.tab-handles');
            if ( ! handleContainer ) {
                handleContainer = document.createElement('div');
                handleContainer.className = 'tab-handles';
                parentAccordion.prepend(handleContainer);
            }

            const tabHandle = document.createElement('p');
            tabHandle.innerText = title || __( 'Sample Title', 'ten321/accordion-block' );
            tabHandle.className = 'tab-handle';
            tabHandle.setAttribute('data-guid',itemId);
            tabHandle.addEventListener('click',(e) => {
                const id = e.target.getAttribute('data-guid');
                document.querySelectorAll('[data-item-id="' + id + '"]')[0].closest('.wp-block').focus();
            })

            if ( handleContainer.querySelectorAll('[data-guid="' + itemId + '"]').length <= 0 ) {
                handleContainer.append(tabHandle);
            } else {
                handleContainer.replaceChild(tabHandle,handleContainer.querySelector('[data-guid="' + itemId + '"]'));
            }
        }

        function guid() {
            let s4 = () => {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            //return id of format 'aaaaaaaa'-'aaaa'-'aaaa'-'aaaa'-'aaaaaaaaaaaa'
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }

        function titleTextControl() {
            return (
                <Panel heading={__('Accordion Item Controls', 'ten321/accordion-block')}>
                    <PanelRow title={__('Accordion Item Title', 'ten321/accordion-block')}>
                        <PanelBody>
                            <TextControl
                                label={__('Title', 'ten321/accordion-block')}
                                value={title}
                                onChange={(newTitle) => setAttributes({title: newTitle})}
                            />
                        </PanelBody>
                    </PanelRow>
                    {inspectorControls()}
                </Panel>
            );
        }

        function inspectorControls() {
            return (
                <InspectorControls>
                    <Panel heading={__('Accordion Item Settings', 'ten321/accordion-block')}>
                        <PanelRow>
                            <PanelBody>
                                <ToggleControl
                                    label={__('Should this item be open by default?', 'ten321/accordion-block')}
                                    checked={open}
                                    onChange={(isOpen) => setAttributes({open: isOpen})}
                                />
                            </PanelBody>
                        </PanelRow>
                        <PanelRow>
                            <PanelBody>
                                <ToggleControl
                                    label={__('Should the first block of content be visible even when this item is collapsed?', 'ten321/accordion-block')}
                                    checked={introBlock}
                                    onChange={(isIntro) => setAttributes({introBlock: isIntro})}
                                />
                            </PanelBody>
                        </PanelRow>
                    </Panel>
                </InspectorControls>
            );
        }

        function titlePlaceholder() {
            return (
                <div className="accordion-item-title" data-item-id={itemId}>
                    {style === 'tabs' &&
                    <p><strong>{title || __('Sample Title', 'ten321/accordion-block')}</strong></p>
                    }
                    {style !== 'tabs' &&
                    <h4>{title || __('Sample Title', 'ten321/accordion-block')}</h4>
                    }
                </div>
            );
        }

        function getInnerBlocks() {
            if (allowedBlocks.length > 0) {
                return (
                    <InnerBlocks template={BLOCKS_TEMPLATE} allowedBlocks={allowedBlocks}/>
                );
            }
            return (
                <InnerBlocks template={BLOCKS_TEMPLATE}/>
            );
        }

        return (
            <li className={className}>
                {isSelected && titleTextControl()}
                {!isSelected && titlePlaceholder()}
                <div>
                    {getInnerBlocks()}
                </div>
            </li>
        );
    },
    save: ({className, attributes}) => {
        return (
            <InnerBlocks.Content/>
        );
    }

})
