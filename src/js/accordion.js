let ten321Accordion = {
    accordions: null,

    init: function () {
        let openItem = 0;
        if (window.location.hash) {
            openItem = window.location.hash.replace('#', '');
            // Check to make sure the hash refers to an accordion item
            if (!document.getElementById(openItem).classList.contains('accordion-item-content')) {
                openItem = 0;
            }
        }

        this.accordions = document.querySelectorAll('.ten321-accordion');

        this.accordions.forEach((accordion) => {
            if (accordion.classList.contains('tabbed-content')) {
                return false;
            }

            const items = accordion.querySelectorAll('.accordion-item');
            let openFirstItem = true;
            if (openItem !== 0 && accordion.querySelectorAll('#' + openItem).length > 0) {
                openFirstItem = false;
            } else if (accordion.querySelectorAll('[aria-expanded="true"]').length > 0) {
                openItem = accordion.querySelectorAll('[aria-expanded="true"]')[0].id;
                openFirstItem = false;
            }

            items.forEach((item) => {
                ten321Accordion.setUpAccordionItem(item, {
                    openItem: openItem,
                    openFirstItem: openFirstItem,
                });

                let handles = accordion.querySelectorAll('.accordion-control-button');

                handles.forEach((handle) => {
                    ten321Accordion.setClickEvents(handle);
                });
            });

            if (openFirstItem) {
                ten321Accordion.openItem(accordion.querySelectorAll('.accordion-item')[0]);
            } else if (openItem !== 0) {
                ten321Accordion.openItem(accordion.querySelector('#' + openItem).closest('.accordion-item'));

                ten321Accordion.scrollTo();
            }
        });

        return false;
    },

    hashChanged: function () {
        if (!window.location.hash) {
            return false;
        }

        const openItem = window.location.hash.replace('#', '');
        const openItemContent = document.getElementById(openItem);
        const accordion = openItemContent.closest('.ten321-accordion');
        const items = accordion.querySelectorAll('.accordion-item');

        let openFirstItem = true;
        if (openItem !== 0 && accordion.querySelectorAll('#' + openItem).length > 0) {
            openFirstItem = false;
        } else if (accordion.querySelectorAll('[aria-expanded="true"]').length > 0) {
            openFirstItem = false;
        }

        items.forEach((item) => {
            ten321Accordion.closeItem(item);
        })

        ten321Accordion.openItem(openItemContent.closest('.accordion-item'));
    },

    setUpAccordionItem: function (item, atts) {
        let openItem = 0;
        let openFirstItem = true;

        if ('openItem' in atts) {
            openItem = atts.openItem;
        }
        if ('openFirstItem' in atts) {
            openFirstItem = atts.openFirstItem;
        }

        const accordion = item.closest('.ten321-accordion');
        let highestHeight = accordion.getAttribute('data-max-height');
        const currentContent = item.querySelector('.accordion-item-content');

        let isExpanded = item.querySelector('.accordion-control-button').getAttribute('aria-expanded') === 'true';

        if (0 !== openItem) {
            isExpanded = openItem === currentContent.id;
        } else if (openFirstItem && accordion.querySelectorAll('.accordion-item:first-child') === item) {
            isExpanded = true;
        }

        // Start with all items expanded, so we can measure their height; if any need to be closed, we'll do that later
        item.querySelector('.accordion-control-button').setAttribute('aria-expanded', 'true');
        item.querySelector('.accordion-item-content').setAttribute('aria-hidden', 'false');

        currentContent.setAttribute('data-max-height', currentContent.offsetHeight);
        if (currentContent.offsetHeight > highestHeight) {
            highestHeight = currentContent.offsetHeight;
            accordion.setAttribute('data-max-height', highestHeight);
        }

        ten321Accordion.closeItem(item);
    },

    closeItem: function (item) {
        item.querySelector('.accordion-item-content').style.maxHeight = 0;
        item.querySelector('.accordion-control-button').setAttribute('aria-expanded', 'false');
        item.querySelector('.accordion-item-content').setAttribute('aria-hidden', 'true');

        item.querySelector('.accordion-item-content').removeEventListener('transitionend', ten321Accordion.transitionFinished);
    },

    openItem: function (item) {
        if (typeof item === 'undefined') {
            return;
        }
        const itemContentElement = item.querySelector('.accordion-item-content');
        itemContentElement.addEventListener('transitionend', ten321Accordion.transitionFinished);

        item.querySelector('.accordion-control-button').setAttribute('aria-expanded', 'true');
        itemContentElement.setAttribute('aria-hidden', 'false');

        if ( itemContentElement.getAttribute('data-max-height') > 0 ) {
            itemContentElement.style.maxHeight = itemContentElement.getAttribute('data-max-height') + 'px';
        } else {
            itemContentElement.style.maxHeight = 'initial';
        }
    },

    setClickEvents: function (handle) {
        handle.removeEventListener('click', ten321Accordion.doClick);
        handle.addEventListener('click', ten321Accordion.doClick);
    },

    doClick: function (e) {
        let control = e.target;
        let accordion = control.closest('.ten321-accordion');
        let items = accordion.querySelectorAll('.accordion-item');
        let currentItem = control.closest('.accordion-item');
        let currentContent = currentItem.querySelector('.accordion-item-content');
        let highestHeight = accordion.getAttribute('data-max-height');
        let isClosed = currentContent.getAttribute('aria-hidden') === 'true';

        const state = {
            pageId: window.location.href.split("#")[0],
            accordionId: Array.prototype.indexOf.call(document.querySelectorAll('.ten321-accordion'), accordion),
            itemId: Array.prototype.indexOf.call(items, currentItem),
            htmlId: currentContent.id,
        };

        items.forEach((item) => {
            ten321Accordion.closeItem(item);
        });

        if (isClosed) {
            currentContent.addEventListener('transitionend', ten321Accordion.transitionFinished);

            ten321Accordion.openItem(currentItem);

            history.pushState(state, control.innerText, '#' + state.htmlId);
        }
    },

    getScrollTop: function (item) {
        if (typeof item === 'undefined') {
            return false;
        }

        const headers = document.querySelectorAll('.site-header');
        let stickyHeader = false;
        headers.forEach((header) => {
            let compStyle = getComputedStyle(header);
            let stylePosition = compStyle.getPropertyValue('position');
            if (stylePosition === 'fixed') {
                stickyHeader = header;
            }
        });

        let abOffset = 0;
        const adminBar = document.getElementById('wpadminbar');
        if (null !== adminBar) {
            let abStyle = getComputedStyle(adminBar);
            let abPosition = abStyle.getPropertyValue('position');
            if (abPosition === 'fixed') {
                abOffset = adminBar.offsetHeight;
            }
        }

        let pos = window.pageYOffset + item.getBoundingClientRect().top;
        if (stickyHeader !== false) {
            pos -= stickyHeader.offsetHeight;
        }
        pos -= abOffset;

        return pos;
    },

    scrollTo: function () {
        if (!window.location.hash) {
            return;
        }

        const item = document.getElementById(window.location.hash.replace('#', '')).closest('.accordion-item');
        if (typeof item === 'undefined') {
            return;
        }

        const scrollTop = ten321Accordion.getScrollTop(item);
        if (false === scrollTop || isNaN(scrollTop)) {
            return;
        }

        window.scrollTo({
            top: scrollTop,
            behavior: 'smooth',
        });

        item.querySelector('.accordion-control-button').focus();
    },

    transitionFinished: function (e) {
        const openItem = e.target.closest('.accordion-item');
        e.target.removeEventListener('transitionend', ten321Accordion.transitionFinished);

        ten321Accordion.scrollTo();
    },

    popStateChanged: function (e) {
        if (typeof e === 'undefined') {
            ten321Accordion.hashChanged();
            return false;
        }

        if ('htmlId' in e.state) {
            const accordion = document.getElementById(e.state.htmlId).closest('.ten321-accordion');
            const currentItem = document.getElementById(e.state.htmlId).closest('.accordion-item');

            accordion.querySelectorAll('.accordion-item').forEach((item) => {
                ten321Accordion.closeItem(item);
            });

            ten321Accordion.openItem(currentItem);

            ten321Accordion.scrollTo();
        }
    },
};

document.addEventListener('DOMContentLoaded', function () {
    ten321Accordion.init();
});

window.onhashchange = ten321Accordion.hashChanged();

window.addEventListener('popstate', function () {
    ten321Accordion.popStateChanged();
});
