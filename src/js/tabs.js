let ten321Tabs = {
    index: 0,
    tabs: [],
    tabGroups: [],

    init: function () {
        this.tabGroups = document.querySelectorAll('.tabbed-content');
        this.tabs = document.querySelectorAll('a.tab');

        this.tabGroups.forEach((group) => {
            if (group.querySelectorAll('a.tab.selected').length <= 0) {
                const first = group.querySelector('.tab-handle:first-child a.tab');
                ten321Tabs.setFocus(group, first);
            }
            if (group.querySelectorAll('a.tab.selected').length > 1) {
                const first = group.querySelectorAll('.tab-handle.current a.tab')[0];
                ten321Tabs.setFocus(group, first);
            }
            const tabs = group.querySelectorAll('a.tab');
            group.setAttribute('data-max-height', 0);
            tabs.forEach((tab) => {
                let tabContent = document.querySelector(tab.getAttribute('href'));

                const isHidden = tabContent.classList.contains('hidden');

                tabContent.classList.remove('hidden');
                tabContent.classList.add('current');

                tabContent.style.maxHeight = 'initial';
                let compStyle = getComputedStyle(tabContent);
                if (compStyle.getPropertyValue('height') > tab.closest('.tabbed-content').getAttribute('data-max-height')) {
                    tab.closest('.tabbed-content').setAttribute('data-max-height', compStyle.getPropertyValue('height'));
                }
                tabContent.style.maxHeight = '';

                if (isHidden) {
                    tabContent.classList.add('hidden');
                    tabContent.classList.remove('current');
                }

                tab.addEventListener('keydown', ten321Tabs.keyDown);
                tab.addEventListener('click', ten321Tabs.click);
            });

            group.querySelectorAll('.tab-panel').forEach((panel) => {
                const tabGroup = group.closest('.tabbed-content');
                panel.style.height = tabGroup.getAttribute('data-max-height');
            });
        });
    },

    keyDown: function (ev) {
        const selected = ev.target;

        const LEFT_ARROW = 37;
        const UP_ARROW = 38;
        const RIGHT_ARROW = 39;
        const DOWN_ARROW = 40;

        let k = ev.which || ev.keyCode;

        const tabGroup = selected.closest('.tabbed-content');
        const current = tabGroup.querySelector('a.tab[aria-selected="true"]');
        const tabs = tabGroup.querySelectorAll('a.tab');
        let newTab = null;

        // if the key pressed was an arrow key
        if (k >= LEFT_ARROW && k <= DOWN_ARROW) {
            // move left one tab for left and up arrows
            if (k === LEFT_ARROW || k === UP_ARROW) {
                if (tabs.item(0) === current) {
                    newTab = tabGroup.querySelector('.tab-handle:last-child a.tab');
                } else {
                    let tmp = current.parentNode.previousElementSibling;
                    newTab = tmp.querySelector('a.tab');
                }
            }

            // move right one tab for right and down arrows
            else if (k === RIGHT_ARROW || k === DOWN_ARROW) {
                if (tabGroup.querySelector('.tab-handle:last-child a.tab') === current) {
                    newTab = tabGroup.querySelector('.tab-handle:first-child a.tab');
                } else {
                    let tmp = current.parentNode.nextElementSibling;
                    newTab = tmp.querySelector('a.tab');
                }
            }

            // trigger a click event on the tab to move to
            ten321Tabs.setFocus(tabGroup, newTab);
            ev.preventDefault();
        }
    },

    click: function (ev) {
        ten321Tabs.setFocus(ev.target.closest('.tabbed-content'), ev.target);
        ev.preventDefault();
        return false;
    },

    setFocus: function (tabGroup, selected) {
        const tabs = tabGroup.querySelectorAll('a.tab');
        const panels = tabGroup.querySelectorAll('.tab-panel');

        // undo tab control selected state,
        // and make them not selectable with the tab key
        // (all tabs)
        tabs.forEach((tab) => {
            tab.setAttribute('tabindex', '-1');
            tab.setAttribute('aria-selected', 'false');
            tab.classList.remove('selected');
            tab.closest('li').classList.remove('current');
        });

        panels.forEach((panel) => {
            panel.classList.remove('current');
            panel.classList.add('hidden');
        });

        // make the selected tab the selected one, shift focus to it
        selected.setAttribute('tabindex', '0');
        selected.setAttribute('aria-selected', 'true');
        selected.classList.add('selected');
        selected.focus();
        selected.closest('li').classList.add('current');

        document.querySelector(selected.getAttribute('href')).classList.add('current');
        document.querySelector(selected.getAttribute('href')).classList.remove('hidden');

        return false;
    }
};

document.addEventListener('DOMContentLoaded', function () {
    ten321Tabs.init();
});
